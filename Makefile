DIR_PATH := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
YAML_FILE = $(DIR_PATH)docker-compose.yaml
ENV_FILE = $(DIR_PATH).env

ini_volumes:
	mkdir -p "./docker/_volumes/postgres"

start:
	docker-compose -f $(YAML_FILE) --env-file $(ENV_FILE) up --build -d \
	&& docker exec -it -e COMPOSER_MEMORY_LIMIT=-1 backend composer install

stop:
	docker-compose -f $(YAML_FILE) --env-file $(ENV_FILE) stop

ls:
	docker ps -a

rm:
	docker rm -f frontend backend storage

analyse-b:
	docker exec -it backend php -d memory_limit=-1 vendor/bin/phpstan analyse -c phpstan.neon

cache:
	docker exec -it -e COMPOSER_MEMORY_LIMIT=-1 backend composer install
