<?php

declare(strict_types=1);


use PHPUnit\Framework\TestCase;

class HelloWorldTest extends TestCase
{
    public function testHello():void
    {
        $this->assertSame('hello, world', 'hello, world');
    }
}
