import React, { type FC } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Box, Container, CssBaseline } from '@mui/material';

import { Footer } from 'widgets/Footer';
import { Header } from 'widgets/Header';

import './styles/index.scss';

import { RouterProvider } from './providers/RouterProvider';
import { StoreProvider } from './providers/StoreProvider';
import { ThemeProvider } from './providers/ThemeProvider';

const App: FC = () => (
  <StoreProvider>
    <BrowserRouter>
      <ThemeProvider>
        <CssBaseline />
        <Box sx={{ minHeight: '100vh', display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
          <Header />
          <Container maxWidth="lg" sx={{ flexGrow: 1 }}>
            <RouterProvider />
          </Container>
          <Footer />
        </Box>
      </ThemeProvider>
    </BrowserRouter>
  </StoreProvider>
);

export default App;
