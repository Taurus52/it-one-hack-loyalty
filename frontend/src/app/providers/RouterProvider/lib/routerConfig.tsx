import { type RouteProps } from 'react-router-dom';

import { MainPage } from 'pages/MainPage';
import { NotFoundPage } from 'pages/NotFoundPage';
import { RulesPage } from 'pages/RulesPage';
import { UsersPage } from 'pages/UsersPage';

import { ERouteName, ROUTE_PATH } from 'shared/config/routePath/routePath';

export const ROUTER_CONFIG: Record<ERouteName, RouteProps> = {
  [ERouteName.MAIN]: {
    path: ROUTE_PATH.MAIN,
    element: <MainPage />,
  },
  [ERouteName.USERS]: {
    path: ROUTE_PATH.USERS,
    element: <UsersPage />,
  },
  [ERouteName.RULES]: {
    path: ROUTE_PATH.RULES,
    element: <RulesPage />,
  },
  [ERouteName.NOT_FOUND]: {
    path: ROUTE_PATH.NOT_FOUND,
    element: <NotFoundPage />,
  },
};
