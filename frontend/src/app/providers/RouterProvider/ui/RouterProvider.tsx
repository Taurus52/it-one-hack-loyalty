import { type FC, Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';
import { Grid } from '@mui/material';

import { ROUTER_CONFIG } from '../lib/routerConfig';

export const RouterProvider: FC = () => (
  <Suspense fallback={<>Loading...</>}>
    <Routes>
      {Object.values(ROUTER_CONFIG).map(({ path, element }) => (
        <Route
          key={path}
          path={path}
          element={
            <Grid container spacing={2}>
              {element}
            </Grid>
          }
        />
      ))}
    </Routes>
  </Suspense>
);
