import { useDispatch, useSelector } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';

import { usersApi } from 'entities/AdministratorOffice';
import { breadcrumbReducer } from 'entities/Breadcrumbs';
import { productApi } from 'entities/Product';
import { userReducer } from 'entities/User';
import { userApi } from 'entities/User';

export const store = configureStore({
  reducer: {
    breadcrumbs: breadcrumbReducer,
    user: userReducer,
    [userApi.reducerPath]: userApi.reducer,
    [usersApi.reducerPath]: usersApi.reducer,
    [productApi.reducerPath]: productApi.reducer,
  },
  devTools: __IS_DEV__,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(userApi.middleware, usersApi.middleware, productApi.middleware),
});

export type TRootState = ReturnType<typeof store.getState>;
export type TAppDispatch = typeof store.dispatch;

export const useAppDispatch: () => TAppDispatch = useDispatch;
export const useAppSelector = useSelector;
