import { type IBreadcrumbSchema } from 'entities/Breadcrumbs';

export interface IStoreSchema { // Непонятно зачем этот тип нужен если он выводтися (из минусов вижу проблему с поднятием версий, хотя может это быть и плюсом тут хз)
  breadcrumbs: IBreadcrumbSchema;
}
