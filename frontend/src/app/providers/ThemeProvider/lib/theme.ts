import { type Breakpoint, createTheme } from '@mui/material';

const breakpoints: Breakpoint[] = ['xs', 'sm', 'md', 'lg', 'xl'];

const values: Record<Breakpoint | number, number> = {
  xs: 0,
  sm: 768,
  md: 960,
  lg: 1280,
  xl: 1920,
};

export const Theme = createTheme({
  components: {
    MuiAppBar: {
      styleOverrides: {
        root: {
          background: 'white',
          color: 'black',
        },
      },
    },
  },
  palette: {
    common: { black: '#000', white: '#fff' },
    background: { paper: 'rgba(238, 238, 238, 1)', default: 'rgba(255, 255, 255, 1)' },
    primary: {
      light: 'rgba(195, 237, 110, 1)',
      main: 'rgba(170, 230, 50, 1)',
      dark: 'rgba(141, 200, 25, 1)',
      contrastText: 'rgba(0, 0, 0, 1)',
    },
    secondary: {
      light: 'rgba(182, 184, 185, 1)',
      main: 'rgba(150, 152, 154, 1)',
      dark: 'rgba(120, 123, 125, 1)',
      contrastText: 'rgba(255, 255, 255, 1)',
    },
    error: {
      light: 'rgba(191, 26, 47, 1)',
      main: 'rgba(191, 26, 47, 1)',
      dark: 'rgba(191, 26, 47, 1)',
      contrastText: '#fff',
    },
    text: {
      primary: 'rgba(0, 0, 0, 1)',
      secondary: 'rgba(150, 152, 154, 1)',
      disabled: 'rgba(187, 187, 187, 1)',
    },
  },
  typography: {
    fontFamily: ['Lab Grotesque', 'Arial', 'serif'].join(','),
  },
  breakpoints: {
    keys: breakpoints,
    up: (key) => `@media (min-width:${values[key]}px)`,
  },
});
