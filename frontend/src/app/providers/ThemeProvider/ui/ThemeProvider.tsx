import React, { type FC } from 'react';
import { ThemeProvider as MaterialThemeProvider } from '@mui/material';

import { Theme } from '../lib/theme';

export const ThemeProvider: FC = ({ children }) => (
  <MaterialThemeProvider theme={Theme}>{children}</MaterialThemeProvider>
);
