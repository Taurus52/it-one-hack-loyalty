declare module '*.module.scss' {
  type TClassNames = Record<string, string>;
  const ClassName: TClassNames;
  export = ClassName;
}

declare module '*.svg' {
  import type React from 'react';

  const SVG: React.VFC<React.SVGProps<SVGSVGElement>>;
  export default SVG;
}

declare module '*.png' {
  const png: string;
  export default png;
}

declare module '*.jpg' {
  const jpg: string;
  export default jpg;
}

declare module '*.woff2' {
  const woff2: string;
  export default woff2;
}
declare module '*.eot' {
  const eot: string;
  export default eot;
}
declare module '*.ttf' {
  const ttf: string;
  export default ttf;
}
declare module '*.woff' {
  const woff: string;
  export default woff;
}

declare const __BASE_URL__: string;
declare const __IS_DEV__ = false;
