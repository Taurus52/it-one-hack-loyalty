import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

import type { IUser } from '../model/user';

export const usersApi = createApi({
  reducerPath: 'administratorOffice/usersApi',
  baseQuery: fetchBaseQuery({ baseUrl: __BASE_URL__ }),
  endpoints: (builder) => ({
    getUsers: builder.query<{ items: IUser[]; count: number }, { page: number; pageSize: number }>({
      query({ page, pageSize }) {
        return `/users?_page=${page + 1}&_limit=${pageSize}`;
      },
      transformResponse(items: IUser[], meta) {
        return {
          items,
          count: Number(meta?.response?.headers.get('X-Total-Count')),
        };
      },
    }),
  }),
});

export const { useGetUsersQuery } = usersApi;
