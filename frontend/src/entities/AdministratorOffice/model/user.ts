import {type TAnyRole} from 'shared/model/roles';

export interface IUser {
    UUID: string,
    name: string,
    login: string,
    avatar: string,
    balance: number,
    updatedAt: string,
    isActive: boolean,
    deactivatedAt?: string,
    roles: TAnyRole[],
}