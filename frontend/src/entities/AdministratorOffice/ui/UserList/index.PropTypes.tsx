import { type SetStateAction } from 'react';

import { type IUser } from 'entities/AdministratorOffice/model/user';

export interface IProps {
    users: IUser[],
    usersCount: number,
    isLoading: boolean,
    page: number,
    pageSize: number,
    setPage: (pageSize: number | SetStateAction<number>) => void,
    canDiactivate: boolean,
}