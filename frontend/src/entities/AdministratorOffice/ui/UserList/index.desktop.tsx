import { DataGrid } from '@mui/x-data-grid';

import { type IUser } from 'entities/AdministratorOffice/model/user';

import type { IProps } from './index.PropTypes';

export function UserList({ users, usersCount, page, setPage, pageSize, isLoading }: IProps) {
  return (
    <DataGrid
      rows={users}
      rowCount={usersCount}
      columns={columnsDef}
      getRowId={getRowId}
      paginationMode="server"
      paginationModel={{
        pageSize,
        page,
      }}
      loading={isLoading}
      onPaginationModelChange={(model) => {
        setPage(model.page);
      }}
    />
  );
}

function getRowId({ UUID }: IUser) {
  return UUID;
}

interface IUserColumnsDef {
  field: keyof IUser;
  headerName: string;
  type?: 'boolean';
  editable?: boolean;
}

const userColumnDef: IUserColumnsDef[] = [
  {
    field: 'UUID',
    headerName: 'Id пользователя',
  },
  {
    field: 'updatedAt',
    headerName: 'Дата изменения',
  },
  {
    field: 'name',
    headerName: 'Фамилия',
  },
  {
    field: 'login',
    headerName: 'Логин',
  },
  {
    field: 'isActive',
    headerName: 'Неактивен',
    type: 'boolean',
    editable: false,
  },
  {
    field: 'deactivatedAt',
    headerName: 'Дата деактивации',
  },
];

const columnsDef = bildColumns(userColumnDef);

function bildColumns(userColumnDef: IUserColumnsDef[]) {
  return userColumnDef.map(({ field, headerName, type, editable }) => ({
    field,
    headerName,
    sortable: false,
    filterable: false,
    type,
    editable,
  }));
}
