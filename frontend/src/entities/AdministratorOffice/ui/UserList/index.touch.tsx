import { Fragment } from 'react';
import { Button } from '@mui/material';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';

import type { IProps } from './index.PropTypes';

export function UserList({ users, usersCount, page, setPage, pageSize, isLoading }: IProps) {
  const hasItemsToLoad = usersCount > (page + 1) * pageSize;

  return (
    <List sx={{ width: '100%', backgroundColor: (theme) => theme.palette.background.paper }}>
      {users?.map((user) => (
        <Fragment key={user.UUID}>
          <ListItem alignItems="flex-start">
            <ListItemAvatar>
              <Avatar alt={user.name} src={user.avatar} />
            </ListItemAvatar>
            <ListItemText
              primary={user.login}
              secondary={
                <>
                  <Typography variant="subtitle2" color="textPrimary">
                    {user.name}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    Обновили: {user.updatedAt}
                  </Typography>
                  {!user.isActive && (
                    <Typography variant="body2" color="textSecondary">
                      Дата удаления: {user.deactivatedAt}
                    </Typography>
                  )}
                </>
              }
            />
          </ListItem>
          <Divider variant="inset" component="li" />
        </Fragment>
      ))}
      {hasItemsToLoad && !isLoading && (
        <ListItem alignItems="center" sx={{ justifyContent: 'center' }}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              setPage((page) => page + 1);
            }}
          >
            Загрузить еще
          </Button>
        </ListItem>
      )}
    </List>
  );
}
