import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

import { UserList as UserListDesktop } from './index.desktop';
import type { IProps } from './index.PropTypes';
import { UserList as UserListTouch } from './index.touch';

export function UserList(props: IProps) {
  const theme = useTheme();
  const idSmallView = useMediaQuery(theme.breakpoints.down('sm'));

  if (idSmallView) {
    return <UserListTouch {...props} />;
  }

  return <UserListDesktop {...props} />;
}
