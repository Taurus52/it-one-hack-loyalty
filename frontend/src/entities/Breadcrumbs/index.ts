export { breadcrumbActions, breadcrumbReducer } from './model/slice/breadcrumbSlice';
export { Breadcrumbs } from './ui/Breadcrumbs/Breadcrumbs';
export type { IBreadcrumbSchema } from './model/types/Breadcrumbs';
export { useBreadcrumbs } from './lib/hooks/useBreadcrumbs';
