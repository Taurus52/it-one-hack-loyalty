import { useEffect } from 'react';

import { useAppDispatch } from 'app/providers/StoreProvider/lib/storeConfig';

import { ERouteName, ROUTE_PATH } from 'shared/config/routePath/routePath';

import { breadcrumbActions } from '../../model/slice/breadcrumbSlice';
import { type IBreadcrumb } from '../../model/types/Breadcrumbs';

const BREADCRUMBS_NAMES: Record<ERouteName, string> = {
  [ERouteName.MAIN]: 'Главная',
  [ERouteName.USERS]: 'Кабинет администратора',
  [ERouteName.RULES]: 'Правила начисления баллов',
  [ERouteName.NOT_FOUND]: '',
};

const getBreadcrumbFromEnum = (routeEnum: ERouteName): IBreadcrumb => ({
  text: BREADCRUMBS_NAMES[routeEnum],
  to: ROUTE_PATH[routeEnum],
});

export const useBreadcrumbs = (items?: ERouteName[]) => {
  const appDispatch = useAppDispatch();
  useEffect(() => {
    if (items) {
      appDispatch(breadcrumbActions.setBreadcrumbs(items.map(getBreadcrumbFromEnum)));
    }
  }, [appDispatch, items]);
};
