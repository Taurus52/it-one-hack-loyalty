import { createSelector } from '@reduxjs/toolkit';

import { type TRootState } from 'app/providers/StoreProvider/lib/storeConfig';

export const selectBreadcrumbsItems = (state: TRootState) => state.breadcrumbs;
export const selectBreadcrumbs = createSelector(selectBreadcrumbsItems, (state) => state.items);
