import { createSlice, type PayloadAction } from '@reduxjs/toolkit';

import { type IBreadcrumb, type IBreadcrumbSchema } from '../types/Breadcrumbs';

const initialState: IBreadcrumbSchema = {
  items: [],
};

export const breadcrumbSlice = createSlice({
  name: 'breadcrumbSlice',
  initialState,
  reducers: {
    setBreadcrumbs: (state, action: PayloadAction<IBreadcrumb[]>) => {
      state.items = action.payload;
    },
  },
});

export const { actions: breadcrumbActions, reducer: breadcrumbReducer } = breadcrumbSlice;
