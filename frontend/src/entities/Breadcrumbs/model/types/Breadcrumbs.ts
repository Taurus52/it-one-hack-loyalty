export interface IBreadcrumbSchema {
  items: IBreadcrumb[];
}

export interface IBreadcrumb {
  text: string;
  to: string;
}
