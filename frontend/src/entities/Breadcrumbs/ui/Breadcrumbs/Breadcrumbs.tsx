import { type FC } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Breadcrumbs as MaterialBreadcrumbs, Link as MaterialLink, Typography } from '@mui/material';

import { useAppSelector } from 'app/providers/StoreProvider/lib/storeConfig';

import { selectBreadcrumbs } from '../../model/selectors/breadcrumbSelector';

export const Breadcrumbs: FC = () => {
  const breadcrumbs = useAppSelector(selectBreadcrumbs);
  return (
    <>
      {breadcrumbs.length && (
        <MaterialBreadcrumbs>
          {breadcrumbs.map((bread, index) =>
            index + 1 === breadcrumbs.length ? (
              <Typography key={bread.to} color="primary">
                {bread.text}
              </Typography>
            ) : (
              <MaterialLink key={bread.to} component={RouterLink} to={bread.to}>
                {bread.text}
              </MaterialLink>
            ),
          )}
        </MaterialBreadcrumbs>
      )}
    </>
  );
};
