export { Product } from './ui/Product/Product';
export { productApi, useGetProductsQuery, useGetProductByIdQuery } from './model/api/ProductApi';
