export interface IProduct {
  name: string;
  description: string;
  cost: number;
  image: string;
  params: string[];
}
