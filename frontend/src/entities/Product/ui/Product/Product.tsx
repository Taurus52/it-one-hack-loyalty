import { type FC } from 'react';
import { Box, Button, Card, CardContent, CardMedia, Typography } from '@mui/material';

import Coin from 'shared/assets/icons/coin.svg';

import { type IProduct } from '../../model/types/product';

export const Product: FC<IProduct> = ({ name, image, description, cost, params }) => (
  <Card
    sx={{
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      background: (theme) => theme.palette.common.white,
    }}
  >
    <div>
      <CardMedia image={image} sx={{ height: 150 }} title={name} />
      <CardContent>
        <Typography variant="h5" mb={1}>
          {name}
        </Typography>
        <Typography variant="body2" mb={1}>
          {description}
        </Typography>
        {params.map((param) => (
          <Typography variant="subtitle2" key={param}>
            {param}
          </Typography>
        ))}
      </CardContent>
    </div>
    <CardContent>
      <Box display="flex" justifyContent="flex-end" alignItems="center" mt={1} mb={2}>
        <Typography mr={1}>{cost}</Typography>
        <Coin />
      </Box>
      <Button variant="contained" fullWidth>
        Заказать
      </Button>
    </CardContent>
  </Card>
);
