export const getInitialsFromUserName = (name: string | undefined) => {
  if (name) {
    const arr = name.split(' ');
    const firstTwo = arr.slice(0, 2);
    const initials = firstTwo.map((item) => item.slice(0, 1));

    return initials.join('').toUpperCase();
  }
};
