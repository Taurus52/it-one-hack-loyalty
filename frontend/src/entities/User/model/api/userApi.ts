import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

import { type IUser } from 'entities/User';

export const userApi = createApi({
  reducerPath: 'userApi',
  baseQuery: fetchBaseQuery({
    baseUrl: __BASE_URL__,
  }),
  endpoints: (builder) => ({
    getCurrentUser: builder.query<IUser, void>({
      query: () => `/currentUser`,
    }),
  }),
});

export const { useGetCurrentUserQuery } = userApi;
