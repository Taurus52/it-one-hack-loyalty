import { createSelector } from '@reduxjs/toolkit';

import { type TRootState } from 'app/providers/StoreProvider/lib/storeConfig';

export const userSelector = (state: TRootState) => state.user.data;
export const userRoles = createSelector(userSelector, (state) => state?.roles ?? []);
