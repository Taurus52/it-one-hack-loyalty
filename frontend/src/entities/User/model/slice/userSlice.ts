import { createSlice } from '@reduxjs/toolkit';

import { type IUserSchema } from '../types/user';

const initialState: IUserSchema = {
  isLoading: false,
};

export const userSlice = createSlice({
  name: 'userSlice',
  initialState,
  reducers: {},
});

export const { actions: userActions, reducer: userReducer } = userSlice;
