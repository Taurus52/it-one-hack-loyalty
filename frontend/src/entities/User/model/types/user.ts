export interface IUserSchema {
  data?: IUser;
  isLoading: boolean;
  error?: string;
}

export interface IUser {
  id: string;
  name: string;
  balance: number;
  roles: ERole[];
}

export enum ERole {
  ADMIN = 'ADMIN',
  USER = 'USER',
}
