import { type FC } from 'react';
import { Box, Typography, useMediaQuery } from '@mui/material';
import { useTheme } from '@mui/material/styles';

import Coin from 'shared/assets/icons/coin.svg';
import { Avatar } from 'shared/ui/Avatar';

import { getInitialsFromUserName } from '../../lib/utils/getInitialsFromUserName';
import { useGetCurrentUserQuery } from '../../model/api/userApi';

export const User: FC = () => {
  const { data } = useGetCurrentUserQuery();
  const theme = useTheme();
  const isXs = useMediaQuery(theme.breakpoints.down('sm'));
  if (data) {
    return (
      <Box display="flex" alignItems="center" sx={{ padding: (theme) => theme.spacing(1) }}>
        <Avatar>{getInitialsFromUserName(data?.name)}</Avatar>
        <Box sx={{ padding: (theme) => theme.spacing(1) }}>
          <Typography fontSize={isXs ? 12 : 'inherit'}>{data?.name}</Typography>
          <Box display="flex" alignItems="center">
            <Typography color="primary" sx={{ mr: 2 }}>
              Баланс: {data?.balance}
            </Typography>
            <Coin />
          </Box>
        </Box>
      </Box>
    );
  }

  return null;
};
