import {useState} from 'react';

import { type IUser, useGetUsersQuery, UserList } from 'entities/AdministratorOffice';

const noneUsers: IUser[] = [];

export interface IManageUserListPops {
    pageSize: number,
};

export function ManageUserList({pageSize}: IManageUserListPops) {
    const [page, setPage] = useState<number>(0);
    const {data, isFetching} = useGetUsersQuery({
        page,
        pageSize,
    });

    return <UserList
        users={data?.items ?? noneUsers}
        usersCount={data?.count ?? 0}
        isLoading={isFetching}
        page={page}
        setPage={setPage}
        pageSize={pageSize}
        canDiactivate={false}
    />;
}