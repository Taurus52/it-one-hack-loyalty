import { type FC } from 'react';
import { Grid, Skeleton } from '@mui/material';

import { Product, useGetProductsQuery } from 'entities/Product';

export const ProductList: FC = () => {
  const { data } = useGetProductsQuery();
  if (data) {
    return (
      <>
        {data.map((product) => (
          <Grid key={product.name} item xs={12} sm={6} md={4} lg={3}>
            <Product {...product} />
          </Grid>
        ))}
      </>
    );
  }

  return <Skeleton variant="rectangular" width={'100%'} height={'100%'} />;
};
