import { type FC } from 'react';

import { ProductList } from 'features/ProductList/ui/ProductList';

import { useBreadcrumbs } from 'entities/Breadcrumbs';

import { ERouteName } from 'shared/config/routePath/routePath';

const MainPage: FC = () => {
  useBreadcrumbs([ERouteName.MAIN]);
  return (
    <>
      <ProductList />
    </>
  );
};

export default MainPage;
