import { type FC } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Grid, Link, Typography } from '@mui/material';

import cls from 'shared/ui/PageError/ui/PageError.module.scss';

const NotFoundPage: FC = () => (
  <>
    <Grid container justifyContent={'center'} alignItems={'center'} className={cls.PageError}>
      <Typography variant={'h2'} align={'center'}>
        К сожалению, мы не нашли то, что вы искали.
        <br />
        <Link component={RouterLink} to={''}>
          Перейти на главную
        </Link>
      </Typography>
    </Grid>
  </>
);

export default NotFoundPage;
