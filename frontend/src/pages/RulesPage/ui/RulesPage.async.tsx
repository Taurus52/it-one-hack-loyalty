import React from 'react';

export const RulesPageAsync = React.lazy(async () => import('./RulesPage'));
