import { type FC } from 'react';
import { Typography } from '@mui/material';

import { ErrorBoundary } from 'widgets/ErrorBoundary';

import { useBreadcrumbs } from 'entities/Breadcrumbs';

import { ERouteName } from 'shared/config/routePath/routePath';

const RulesPage: FC = () => {
  useBreadcrumbs([ERouteName.MAIN, ERouteName.RULES]);

  return (
      <ErrorBoundary>
        <Typography>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque blanditiis, deleniti dolores ea fugit hic,
          impedit ipsam iste omnis quisquam sequi, tempora ullam vel! Accusamus ad deleniti dolorum id impedit, ipsum
          laboriosam molestias qui, reiciendis repudiandae sapiente vel? Autem deserunt dolore ipsa labore molestiae
          officia qui quos rerum vero vitae voluptas, voluptates voluptatum. Commodi culpa impedit maiores pariatur quo
          sed, tempora voluptate. Accusamus, consequatur delectus deleniti deserunt dolore dolorem eaque, earum, esse
          fuga in laudantium maxime nam odio quasi rem sequi ullam? Aliquam beatae culpa cupiditate, deleniti eaque ex
          illo incidunt inventore ipsa minima modi molestiae mollitia nobis odio praesentium rem ut. Ad blanditiis
          consequatur, corporis cupiditate debitis, deserunt dicta earum eius et, eum eveniet ipsum qui rem repellendus
          similique sint tempore. Accusamus alias aliquam animi asperiores consequuntur delectus doloremque eum eveniet,
          harum impedit ipsam laboriosam nobis obcaecati omnis perspiciatis placeat quam quas qui quis tempora totam
          voluptatem voluptatum! Aliquid ea eos iure laborum necessitatibus nisi perspiciatis. Aperiam atque, dolor
          doloremque itaque maiores neque nesciunt quo repudiandae sed veniam. Ab accusamus atque beatae consectetur
          debitis distinctio dolor dolores eum illo ipsum itaque magni molestiae nobis numquam optio pariatur, quas
          quibusdam repellendus similique unde voluptates voluptatibus voluptatum. Adipisci assumenda commodi
          consectetur eum excepturi hic ipsa, itaque magni minus numquam quia ratione repudiandae sapiente suscipit
          tempore ut vitae. Accusantium, ad aliquid at consequatur dicta dolores fugiat in ipsam minima neque,
          praesentium rerum, voluptatem voluptates. Doloremque ducimus earum, eius eos et exercitationem ipsa ipsum
          laudantium minima minus officiis, quia quisquam quos recusandae rem suscipit temporibus veritatis. Cupiditate
          delectus esse ipsam mollitia quam quidem, soluta tempora. Aspernatur autem cum debitis dicta eveniet ex
          explicabo iste nihil omnis optio praesentium, provident rem, voluptatibus? Dignissimos, earum, neque? Amet
          cum, eius et eum iure nam numquam porro sed voluptatum. Aliquam dignissimos eum excepturi exercitationem
          explicabo possimus quos!
        </Typography>
      </ErrorBoundary>
  );
};

export default RulesPage;
