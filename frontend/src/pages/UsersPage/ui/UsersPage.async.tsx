import React from 'react';

export const UsersPageAsync = React.lazy(async () => import('./UsersPage'));