import { type FC } from 'react';

import { ErrorBoundary } from 'widgets/ErrorBoundary'; // Кажется это можно унести в определение роута DRY

import { ManageUserList } from 'features/AdministratorOffice';

import { useBreadcrumbs } from 'entities/Breadcrumbs';

import { ERouteName } from 'shared/config/routePath/routePath';

const UsersPage: FC = () => {
  useBreadcrumbs([ERouteName.MAIN, ERouteName.USERS]);

  return (
    <ErrorBoundary>
      <ManageUserList pageSize={10}/>
    </ErrorBoundary>
  );
};

export default UsersPage;
