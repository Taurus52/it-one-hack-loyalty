export enum ERouteName {
  MAIN = 'MAIN',
  RULES = 'RULES',
  USERS = 'USERS',
  NOT_FOUND = 'NOT_FOUND',
}
export enum ERouteUrl {
  MAIN = '/',
  RULES = '/rules',
  USERS = '/users',
  NOT_FOUND = '*',
}
export const ROUTE_PATH: Record<ERouteName, ERouteUrl> = {
  [ERouteName.MAIN]: ERouteUrl.MAIN,
  [ERouteName.USERS]: ERouteUrl.USERS,
  [ERouteName.RULES]: ERouteUrl.RULES,
  [ERouteName.NOT_FOUND]: ERouteUrl.NOT_FOUND,
};
