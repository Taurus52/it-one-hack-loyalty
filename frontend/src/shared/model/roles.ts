// Кажется это cross-cutting context нужно обсудить!
export const UserRole = "Пользователь";
export const AdministratorRole = "Администратор";

export type TUserRole = typeof UserRole;
export type TAdministratorRole = typeof AdministratorRole;
export type TAnyRole = TUserRole | TAdministratorRole;

export const Roles = [UserRole, AdministratorRole];