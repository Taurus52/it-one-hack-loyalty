import { type FC } from 'react';
import { Avatar as MaterialAvatar, type AvatarProps, Skeleton } from '@mui/material';

export interface IAvatarProps extends AvatarProps {
  isLoading?: boolean;
}

export const Avatar: FC<IAvatarProps> = ({ isLoading, children, ...avatarProps }) =>
  isLoading ? (
    <Skeleton>
      <MaterialAvatar {...avatarProps}>{children}</MaterialAvatar>
    </Skeleton>
  ) : (
    <MaterialAvatar {...avatarProps}>{children}</MaterialAvatar>
  );
