import { type FC } from 'react';
import { Link } from 'react-router-dom';
import { type Theme, useMediaQuery } from '@mui/material';

import LogoDescIcon from '../lib/images/logo/it_one_logo_descriptor_graphics.png';
import LogoIcon from '../lib/images/logo/it_one_logo_graphics.png';

import cls from './Logo.module.scss';

export const Logo: FC = () => {
  const isSm = useMediaQuery((theme: Theme) => theme.breakpoints.down('sm'));
  return (
    <>
      {isSm ? (
        <Link to={'https://www.it-one.ru'} className={cls.Link}>
          <img src={LogoIcon} className={cls.Logo} alt={'IT-ONE technologies'} />
        </Link>
      ) : (
        <Link to={'https://www.it-one.ru'} className={cls.Link}>
          <img src={LogoDescIcon} className={cls.Logo} alt={'IT-ONE technologies'} />
        </Link>
      )}
    </>
  );
};
