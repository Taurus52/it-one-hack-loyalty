import { type FC } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Grid, Link, Typography } from '@mui/material';

import cls from './PageError.module.scss';

export const PageError: FC = () => {
  const reloadPage = () => {
    location.reload();
  };

  return (
    <Grid container justifyContent={'center'} alignItems={'center'} className={cls.PageError}>
      <Typography variant={'h1'} align={'center'}>
        Что-то пошло не так,
        <br />
        <Link component={RouterLink} to={''} onClick={reloadPage} color="primary">
          перезагрузить страницу
        </Link>
      </Typography>
    </Grid>
  );
};
