import { Component, type ErrorInfo, type ReactElement, type ReactNode } from 'react';

import { PageError } from 'shared/ui/PageError';

interface IErrorBoundaryProps {
  children: ReactNode;
  errorElement?: ReactElement;
}

interface IErrorBoundaryState {
  hasError: boolean;
}

export class ErrorBoundary extends Component<IErrorBoundaryProps, IErrorBoundaryState> {
  static getDerivedStateFromError(_error: Error): IErrorBoundaryState {
    return { hasError: true };
  }

  constructor(props: IErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo): void {
    console.error(error, errorInfo);
  }

  render(): ReactNode {
    if (this.state.hasError) {
      return this.props.errorElement ?? <PageError />;
    }

    return this.props.children;
  }
}
