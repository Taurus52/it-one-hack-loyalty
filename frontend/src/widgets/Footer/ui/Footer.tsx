import { type FC } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Container, Link } from '@mui/material';
import AppBar from '@mui/material/AppBar';

import { ROUTE_PATH } from 'shared/config/routePath/routePath';

export const Footer: FC = () => (
  <AppBar position="static" sx={{ marginTop: (theme) => theme.spacing(2) }}>
    <Container maxWidth="lg" sx={{ display: 'flex', padding: (theme) => theme.spacing(2) }}>
      <Link component={RouterLink} to={ROUTE_PATH.RULES}>
        Правила начисления баллов
      </Link>
      <span>&nbsp;|&nbsp;</span>
      <Link href={'mailto:admin@test.ru?subject=Программа лояльности&body='}>Связь с администратором</Link>
    </Container>
  </AppBar>
);
