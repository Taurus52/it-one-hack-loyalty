import * as React from 'react';
import { type FC } from 'react';
import { useNavigate } from 'react-router-dom';
import MenuIcon from '@mui/icons-material/Menu';
import { Container, Grid, useMediaQuery } from '@mui/material';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import { useTheme } from '@mui/material/styles';

import { Breadcrumbs } from 'entities/Breadcrumbs';
import { User } from 'entities/User';

import { ERouteUrl } from 'shared/config/routePath/routePath';
import { Logo } from 'shared/ui/Logo';
interface INavItem {
  name: string;
  url?: ERouteUrl;
}
const drawerWidth = 240;
const navItems: INavItem[] = [
  { name: 'Каталог товаров', url: ERouteUrl.MAIN },
  { name: 'История транзакций' },
  { name: 'История заказов' },
];

export const Header: FC = () => {
  const navigate = useNavigate();
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setIsMenuOpen((prevState) => !prevState);
  };

  const onNavItemClickHandler = (item: INavItem) => () => {
    navigate(item.url ?? '/');
  };

  const theme = useTheme();
  const isXs = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <>
      <AppBar
        position="sticky"
        sx={{ padding: (theme) => theme.spacing(2), marginBottom: (theme) => theme.spacing(2) }}
      >
        <Container maxWidth="lg">
          <Grid container>
            <Grid
              item
              xs={isXs ? 12 : 4}
              display="flex"
              alignItems="center"
              justifyContent={isXs ? 'center' : 'flex-start'}
            >
              <Logo />
            </Grid>
            <Grid item xs={isXs ? 12 : 8} display="flex" justifyContent={isXs ? 'space-between' : 'flex-end'}>
              <User />

              <Box alignItems="center" display="flex">
                <IconButton onClick={handleDrawerToggle} sx={{ mr: 2, ml: 2 }}>
                  <MenuIcon />
                </IconButton>
              </Box>
            </Grid>
            <Grid container xs={12}>
              <Breadcrumbs />
            </Grid>
          </Grid>
        </Container>
      </AppBar>
      <Drawer
        variant="temporary"
        open={isMenuOpen}
        onClose={handleDrawerToggle}
        anchor="right"
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
        sx={{
          '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
        }}
      >
        <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
          <List>
            {navItems.map((item) => (
              <ListItem key={item.name} disablePadding>
                <ListItemButton sx={{ textAlign: 'center' }}>
                  <ListItemText primary={item.name} onClick={onNavItemClickHandler(item)} />
                </ListItemButton>
              </ListItem>
            ))}
          </List>
        </Box>
      </Drawer>
    </>
  );
};
