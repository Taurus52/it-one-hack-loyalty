/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { faker } from '@faker-js/faker';

faker.locale = 'ru';

const createAdministratorOfficeUser = () => {
  const isActive = faker.datatype.boolean();
  const firstName = faker.name.firstName();
  const lastName = faker.name.lastName();
  return {
    UUID: faker.datatype.uuid(),
    name: faker.name.fullName({ firstName, lastName }),
    login: faker.internet.userName(firstName, lastName),
    avatar: faker.image.avatar(),
    balance: faker.datatype.number({ max: 100 }),
    updatedAt: faker.date.past(),
    isActive,
    deactivatedAt: isActive ? null : faker.date.soon(1),
    roles: faker.helpers.arrayElements(['Пользователь', 'Администратор']),
  };
};

const createProduct = () => ({
  UUID: faker.datatype.uuid(),
  name: faker.commerce.productName(),
  description: faker.commerce.productDescription(),
  cost: faker.datatype.number({ max: 5000 }),
  image: faker.image.imageUrl(300, 150, 'dog', true),
  params: faker.datatype
    .array(faker.datatype.number({ min: 1, max: 5 }))
    .map(() => faker.commerce.productAdjective())
    .reduce((acc: string[], curr: string) => {
      if (!acc.includes(curr)) {
        return [...acc, curr];
      }

      return acc;
    }, []),
});

export const models = {
  users: Array.from({ length: 30 }).map(createAdministratorOfficeUser),
  currentUser: createAdministratorOfficeUser(),
  products: Array.from({ length: 100 }).map(createProduct),
};
