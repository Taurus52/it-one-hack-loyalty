/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import jsonServer from 'json-server';

import { models } from './models';

const server = jsonServer.create();
const router = jsonServer.router(models);
const middlewares = jsonServer.defaults();

server.use(middlewares);
server.use((req, res, next) => {
  setTimeout(() => {
    next();
  }, 1500);
});
server.use(router);
server.listen(5000, () => {
  console.log('JSON Server is running');
});
